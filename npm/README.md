# MC-Banner
```$xslt
超轻量的简易版焦点图, 仅4K
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[https://junbo.name/plugins/mc-banner](https://junbo.name/plugins/mc-banner/)


#### 开始

下载项目: 

```npm
npm i mc-fixtab
```

#### 参数
```$xslt
param               {Object}    入参为一个对象

param.auto          {Boolen}   是否自动播放
param.step          {String}   播放间隔时间
param.dots          {Object}   是否显示圆点
param.btns          {Object}   是否显示前进后退按钮
param.time          {Object}   切换动画时间
param.on            {Object}   回调事件合集
```

#### 示例

```javascript
// 引入插件
import McBanner from 'mc-banner';

new McBanner(document.getElementById('banner'), {
  auto: true,
  step: 5000,
  dots: true,
  btns: true,
  time: 500
});
```

#### 版本
```$xslt
v1.0.0 [Latest version]
1. 核心功能完成。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
