const _class = (el, type, cs) => { let _cs = el.className; let _cns = _cs ? _cs.split(' ') : []; if (type === '+') { if (!_cns.includes(cs)) { _cns.push(cs); el.className = _cns.join(' '); } } if (type === '-') { if (_cns.includes(cs)) { _cns = _cns.filter((i) => { return i !== cs; }); el.className = _cns.join(' '); } } };
const _amend = (function () { var evs = { WebkitAnimation: 'webkitAnimationEnd', animation: 'animationend' }; for (var k in evs) { if (typeof evs[k] === 'string') { return evs[k]; } } })();
const _attr = (el, state) => { el.setAttribute('view', state); };
function McBanner(el, conf) {
  if (el && el.children) {
    // 容器
    this.el = el;
    // 盒子
    this.wrap = el.children[0];
    // 子集
    this.item = this.wrap.children;
    // 数量
    this.len = this.item.length;
    // 自动播放
    this.auto = conf.hasOwnProperty('auto') ? conf.auto : true;
    // 播放间隔
    this.step = conf.step || 5000;
    // 当前活动的子集
    this.index = 0;
    // 是否正在动画中 0=空闲 1=动画
    this.ing = 0;

    this.on = Object.assign({}, conf.on);

    // 自动播放方向
    this.direction = 'l';

    let name = 'mc-banner';
    _class(this.el, '+', name);
    _class(this.wrap, '+', name + '-wrap');

    // 子集大于1才初始化插件
    if (this.len > 1) {
      this.init(conf);
    } else {
      _attr(this.item[0], 3);
    }
  }
}

McBanner.prototype.__idx = function (index) {

  return index < 0 ? this.len - 1 : (index < this.len ? index : 0);
};

McBanner.prototype.init = function (conf) {
  // 是否显示按钮
  let btns = this.el.querySelector('.mc-banner-btns');
  if (btns) {
    this.prev = btns.children[0];
    this.next = btns.children[1];
  } else {
    if (conf.btns && !btns) {
      btns = document.createElement('ul');
      btns.className = 'mc-banner-btns';
      btns.innerHTML = '<li data-btn="prev"></li><li data-btn="next"></li>';
      this.el.appendChild(btns);
      this.prev = btns.children[0];
      this.next = btns.children[1];
    }
  }

  // 是否显示圆点
  let dots = this.el.querySelector('.mc-banner-dots');
  if (dots) {
    this.dots = dots.children;
  } else {
    if (conf.dots && !dots) {
      dots = document.createElement('ul');
      dots.className = 'mc-banner-dots';
      for (let i = 0; i < this.len; i++) {
        dots.innerHTML += '<li></li>';
      }
      this.el.appendChild(dots);
      this.dots = dots.children;
    }
  }
  _attr(this.item[this.index], 3);
  _attr(this.dots[this.index], 3);

  let _auto = () => {
    if (this.auto) {
      clearInterval(this.timer);
      this.timer = setInterval(() => {
        let domRect = this.el.getBoundingClientRect();
        if (domRect.top > 0 && domRect.top < window.innerHeight) {
          this.play(this.index + 1);
        }
      }, this.step);

    }
  };
  _auto();

  this.el.onmouseover = () => {
    clearInterval(this.timer);
    this.el.onmouseout = () => {
      _auto();
    };
  };



  // 触屏事件
  let cts = (ev) => { return ev.changedTouches[ev.changedTouches.length - 1].clientX; };
  let pos = 0;
  this.wrap.ontouchstart = (ev) => {
    pos = 0;
    if (ev.target === this.wrap) {
      clearInterval(this.timer);
      let oldX = cts(ev);
      ev.target.ontouchmove = (ev) => {
        ev.preventDefault();
        if (!this.ing) {
          let newX = cts(ev);
          ev.target.ontouchend = () => {
            pos = (newX - oldX);
            if (pos > 10) {
              this.play(-1, _auto);
            }
            if (pos < -10) {
              this.play(1, _auto);
            }
          };
        }
      };
    }
  };
  this.wrap.onmousedown = (ev) => {
    pos = 0;
    if (ev.target === this.wrap) {
      clearInterval(this.timer);
      var oldX = ev.clientX;
      ev.target.onmousemove = (ev) => {
        ev.preventDefault();
        if (!this.ing) {
          var newX = ev.clientX;
          pos = (newX - oldX);
          ev.target.onmouseup = () => {
            if (pos > 10) {
              this.play(-1, _auto);
            }
            if (pos < -10) {
              this.play(1, _auto);
            }
          };
        }
      };
    }
  };
  // 点击按钮
  this.prev.onclick = () => {
    clearInterval(this.timer);
    this.play(-1, _auto);
  };
  this.next.onclick = () => {
    clearInterval(this.timer);
    this.play(1, _auto);
  };
  // 点击圆点
  Array.prototype.slice.call(this.dots).map((dot, index) => {
    dot.onclick = () => {
      clearInterval(this.timer);
      this.play(index - this.index, _auto);
    };
  });
};

McBanner.prototype.play = function (index, cb = () => { }) {
  if (!this.ing) {
    this.ing = 1;
    this.index = this.__idx(this.index + index);
    this.direction = index < 0 ? 'r' : 'l';
    this.wrap.setAttribute('direction', this.direction);

    let previndex = this.__idx(this.index - index);

    for (let i = 0; i < this.len; i++) {
      _attr(this.item[i], 0);
      _attr(this.dots[i], 0);
    }

    _attr(this.item[this.index], 2);
    _attr(this.dots[this.index], 2);
    _attr(this.item[previndex], 1);

    let __end = () => {
      _attr(this.item[this.index], 3);
      _attr(this.dots[this.index], 3);
      this.ing = 0;
      cb();
      this.on.show && this.on.show(this);
    };

    this.item[this.index].addEventListener(_amend, __end, false);
  }
};

export default McBanner;
