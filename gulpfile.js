let gulp = require('gulp');
let clean = require('gulp-clean');
let sass = require('gulp-sass');
let autoprefixer = require('gulp-autoprefixer');
let mincss = require('gulp-clean-css');
let gcmq = require('gulp-group-css-media-queries');

let cleanup = () => {
  return gulp.src([
    './npm/**/*.css'
  ]).pipe(clean());
};

let style = () => {
  return gulp.src([
    './lib/theme/mc-toast@theme=aa.scss',
    './lib/theme/mc-toast@theme=ab.scss',
    './lib/theme/mc-toast@theme=ac.scss'
  ])
    .pipe(sass())
    .pipe(autoprefixer({
      overrideBrowserslist: [
        '> 1%',
        'last 2 versions',
        'not ie <= 10',
        'ios >= 8',
        'android >= 4.0'
      ],
      cascade: false
    }))
    .pipe(gcmq())
    .pipe(mincss())
    .pipe(gulp.dest('./dist/theme'))
    .pipe(gulp.dest('./npm/theme'));

  // .pipe(gulp.dest((file) => {
  //   return file.base;
  // }));
};

// function watch() {
//   gulp.watch('./**/*.scss', style);
// }

// let dev = gulp.series(style, gulp.parallel(watch));
let pro = gulp.series(cleanup, style);

gulp.task('default', pro);
// gulp.task('build', pro);
