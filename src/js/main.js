import '../css/main.scss';

// 代码着色插件
import McTinting from 'mc-tinting';
// 新建一个代码着色实例
new McTinting();

// 实例代码
import McBanner from '../../lib/mc-banner';

new McBanner(document.getElementById('banner'), {
  theme: 'ab',
  auto: 1,
  step: 3000,
  dots: true,
  btns: true,
  time: 500
});
